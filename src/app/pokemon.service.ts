import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from './pokemon';
import { IPokemons } from './pokemons';
import { Species } from './species';

@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    pokemonApiUrlWithLimit = 'https://pokeapi.co/api/v2/pokemon/?limit=493';
    pokemonApiUrl = 'https://pokeapi.co/api/v2/pokemon/';
    speciesUrl = 'https://pokeapi.co/api/v2/pokemon-species/';

    constructor(private http: HttpClient) {}

    getPokemonByName(name: string) {
        return this.http.get<Pokemon>(this.pokemonApiUrl + name);
    }

    getPokemonById(id: number) {
        return this.http.get<Pokemon>(this.pokemonApiUrl + id);
    }

    getAllPokemon() {
        return this.http.get<IPokemons>(this.pokemonApiUrlWithLimit);
    }

    getPokemonSpeciesByName(name: string) {
        return this.http.get<Species>(this.speciesUrl + name);
    }
}
