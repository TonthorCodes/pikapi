// import { Species } from './species';

export class Pokemon {
  id: number;
  name: string;
  types: Array<Object>;
  species: Array<any>;
}

