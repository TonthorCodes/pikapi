export class Species {
    speciesId: number;
    speciesName: string;
    flavor_text_entries: Array<any>;
}
