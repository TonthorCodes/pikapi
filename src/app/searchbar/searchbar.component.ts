import { PokemonObj } from './../pokemon-object';
import { PokemonService } from './../pokemon.service';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'app-searchbar',
    templateUrl: './searchbar.component.html',
    styleUrls: [ './searchbar.component.css' ],
    providers: [ PokemonService ]
})
export class SearchbarComponent implements OnInit {
    static responseItem: any;

    @Input() searchItem: string;

    constructor(private apiCall: PokemonService) {}

    pokemonId: number;
    filledId: string;
    hasTwoTypes: boolean;
    pokemonDescription: any;
    searchId: number;
    array: any;
    inputItem: string;
    pokemons: String[] = [];
    myControl = new FormControl();
    filteredPokemons: Observable<Array<Object>>;
    pokemonName: string;
    pokemonType1: string;
    pokemonType2: string;

    /*
      Called on Init
      , uses getAll to receive all Pokemons from the API
      and stores the names in the String-Array pokemons
    */
    ngOnInit() {
        // this.apiCall.getAllPokemon().subscribe((res) => {
        //   res.results.forEach(element => {
        //   this.pokemons.push(new PokemonObj(JSON.stringify(element)));
        //   });
        // });
        this.apiCall.getAllPokemon().subscribe((res) => {
            res.results.forEach((element) => {
                this.pokemons.push(element['name']);
            });
        });
        console.log(this.pokemons);
        this.filteredPokemons = this.myControl.valueChanges.pipe(startWith(''), map((value) => this._filter(value)));
        this.filledId = '000';
    }

    /*
      splits the flow in either numeric(id) or letter-based(name) search
      , runs the related API-Call to receive a pokemon-object
      and uses it to call buildPokemon()
      -mixed scheme isn't allowed
    */
    callByNameOrId(searchItem: string) {
        if (searchItem) {
            if (!this.IsEmpty(searchItem)) {
                if (this.IsID(searchItem)) {
                    console.log(searchItem + ' ist eine ID');
                    // 10 for mathematical system eg. decimal: 10, Binary: 2
                    this.searchId = Number.parseInt(searchItem, 10);
                    this.apiCall.getPokemonById(this.searchId).subscribe((res) => {
                        this.buildPokemon(res);
                    });
                } else {
                    if (this.IsOnlyLetter(searchItem)) {
                        console.log(searchItem + ' ist ein Name');
                        this.apiCall.getPokemonByName(searchItem).subscribe((res) => {
                            this.buildPokemon(res);
                        });
                    }
                }
            }
        }
    }

    /*
      utility-method
      checks if String is numeric and not null
    */
    IsID(value: any): boolean {
        return value != null && !isNaN(Number(value.toString()));
    }

    /*
      utility-method
      checks if String is empty
    */
    IsEmpty(value: any): boolean {
        return !value || value.length === 0;
    }

    /*
      utility-method
      checks if String consists only of letters
    */
    IsOnlyLetter(value: String): boolean {
        this.array = value.toLowerCase().match(/[0-9]/i);
        if (this.array != null) {
            return false;
        } else {
            return true;
        }
    }

    /*
      checks if id is valid,
      loads the pokemon-parameters into the view
      and fills the pokedex-entry with the english text from the API
    */
    buildPokemon(res: Pokemon) {
        if (res.id <= 493) {
            this.pokemonId = res.id;
            this.filledId = String(this.pokemonId);
            while (this.filledId.length < 3) {
                this.filledId = '0' + this.filledId;
            }
            this.pokemonName = res.name;
            this.pokemonType1 = res.types[0]['type']['name'];
            if (res.types[1]) {
                this.pokemonType2 = res.types[1]['type']['name'];
                this.hasTwoTypes = true;
            } else {
                this.hasTwoTypes = false;
            }
            this.apiCall.getPokemonSpeciesByName(res.species['name']).subscribe((specRes) => {
                for (let i in specRes.flavor_text_entries) {
                    if (specRes.flavor_text_entries[i]['language']['name'] === 'en') {
                        this.pokemonDescription = specRes.flavor_text_entries[i]['flavor_text'];
                    }
                }
            });
        }
    }

    private _filter(value: string): Array<Object> {
        const filterValue = value.toLowerCase();
        return this.pokemons.filter((option) => option.toString().toLowerCase().includes(filterValue));
    }
}
