export class PokemonObj {
  name: String;
  url: String;
  constructor(json: string) {
    this.fillFromJSON(json);
  }
  fillFromJSON(json: string){
    let jsonObj = JSON.parse(json);
    for (let propName in jsonObj) {
      this[propName] = jsonObj[propName];
    }
  }
}
